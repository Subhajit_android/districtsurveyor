package com.dosinfotech.surveyor.district;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;


/**
 * Author        : Arpan Bose(arpanbose@hotmail.com)
 * Reviewed by   : Arpan Bose (arpanbose@hotmail.com)
 * Creation Date : 3/9/2018 1:57 PM
 * Modified on   : 3/9/2018 1:57 PM
 * Dependency    :
 * Description   : Fragment class for dashboard
 * Execute like  :
 **/
public class Dashboard extends Fragment {
    ConstraintLayout main_container;
    LinearLayout nonDigitize, holdingSurvey;
    HoldingSurveyType stype;
    CollapsingToolbarLayout collapsingToolbarLayout;
    AppBarLayout appBarLayout;
    Toolbar toolbar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_dashboard, container, false);
        main_container = v.findViewById(R.id.active_content);

        nonDigitize = v.findViewById(R.id.non_digitize);
        holdingSurvey = v.findViewById(R.id.holding_survey);
        appBarLayout = v.findViewById(R.id.myappbar);
        collapsingToolbarLayout = v.findViewById(R.id.collapsing_toolbar_layout);
        toolbar = v.findViewById(R.id.toolbar);
        ((Activity1) getActivity()).setSupportActionBar(toolbar);

        ((Activity1) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);

        appBarLayout.setExpanded(false, true);
        appBarLayout.addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {
            if (Math.abs(verticalOffset) >= appBarLayout.getTotalScrollRange()) {
                CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) appBarLayout.getLayoutParams();
                params.height = toolbar.getHeight();
                this.appBarLayout.setLayoutParams(params);
            }
        });

        Handler h = new Handler();
        h.postDelayed(() -> {
            menuAnimation();
        }, 300);

        nonDigitize.setOnClickListener(v1 ->{
            AlertDialog.Builder alertbox = new AlertDialog.Builder(getActivity());
            alertbox.setIcon(R.mipmap.ic_launcher);
            alertbox.setTitle("Survey Type")
                    .setItems(R.array.layer_survey_option, (dialog, pos) -> {
                        if (pos==0){
                            startSurvey(SurveyType.NORMAL,null);
                        }else if(pos==1){
                            startSurvey(SurveyType.NORMAL,HoldingSurveyType.LAYER_HOLDING_TAGGING);
                        }
                    });
            alertbox.show();
        });
        holdingSurvey.setOnClickListener(v1 -> listDialog());
        return v;
    }


    @TargetApi(Build.VERSION_CODES.M)
    private void startSurvey(SurveyType surveyType,HoldingSurveyType holdingSurveyType) {
        /*if(holdingSurveyType==HoldingSurveyType.BSUP
        || holdingSurveyType==HoldingSurveyType.HOUSING_FOR_ALL
        || holdingSurveyType==HoldingSurveyType.IIHL){
            Intent selectBsup=new Intent(getActivity(),BsupHfaIhhaSelection.class);
            selectBsup.putExtra("holding_type", holdingSurveyType.toString());
            startActivityForResult(selectBsup,BSUP_HFA_IIHL_SELECTION_CODE);
        }else {

        }*/
        final Intent survey = new Intent(getActivity(), SurveyActivity.class);
        survey.putExtra("survey_type", surveyType.toString());
        if (holdingSurveyType != null) {
            survey.putExtra("holding_type", holdingSurveyType.toString());
        }
        startActivity(survey);

    }

    /**
     * Function to show menu animation
     */
    private void menuAnimation() {
        holdingSurvey.animate().translationY(0).translationX(0).alpha(1f).setDuration(700);
        nonDigitize.animate().translationY(0).translationX(0).alpha(1f).setDuration(700);
    }

    /**
     * Dialog to select geo tagging type
     */
    private void listDialog(){
        AlertDialog.Builder alertbox = new AlertDialog.Builder(getActivity());
        alertbox.setIcon(R.mipmap.ic_launcher);
        alertbox.setTitle("Georeferencing")
                .setItems(R.array.geo_tagging_list, (dialog, pos) -> {
                    if (pos==0){
                        startSurvey(SurveyType.HOLDING,HoldingSurveyType.HOUSE_HOLD);
                    }else if(pos==1){
                        startSurvey(SurveyType.HOLDING,HoldingSurveyType.HOUSING_FOR_ALL);
                    }else if(pos==2){
                        startSurvey(SurveyType.HOLDING,HoldingSurveyType.BSUP);
                    }else if(pos==3){
                        startSurvey(SurveyType.HOLDING,HoldingSurveyType.IIHL);
                    }
                });
        alertbox.show();
    }

}
