package com.dosinfotech.surveyor.district.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.dosinfotech.surveyor.mgm.HoldingStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import java.io.Serializable;
import java.util.Date;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(tableName = "bsup")
public class BSUP implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;
    private int gid;
    @ColumnInfo(name = "phase")
    private int phase;
    @ColumnInfo(name = "name")
    private String name;
    @ColumnInfo(name = "slum_name")
    private String slumName;
    @ColumnInfo(name = "holding_no")
    private String holdingNo;
    @ColumnInfo(name = "ward")
    private int wardNo;
    @ColumnInfo(name = "holding_uid")
    private String holdingUid;
    @ColumnInfo(name = "status")
    private HoldingStatus status=HoldingStatus.NULL;
    @ColumnInfo(name = "image")
    private String image;
    @ColumnInfo(name = "json")
    private String json;
    @ColumnInfo(name = "gps_geom")
    private String gpsGeom;
    @ColumnInfo(name = "remarks")
    private String remarks;
    @ColumnInfo(name = "surveyed")
    private boolean surveyed=false;
    @ColumnInfo(name = "uploaded")
    private boolean uploaded=false;
    @ColumnInfo(name = "changed")
    private boolean changed=false;
    @ColumnInfo(name = "date_time")
    private Date dateTime=new Date();


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPhase() {
        return phase;
    }

    public void setPhase(int phase) {
        this.phase = phase;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlumName() {
        return slumName;
    }

    public void setSlumName(String slumName) {
        this.slumName = slumName;
    }

    public int getGid() {
        return gid;
    }

    public void setGid(int gid) {
        this.gid = gid;
    }

    public int getWardNo() {
        return wardNo;
    }

    public void setWardNo(int wardNo) {
        this.wardNo = wardNo;
    }

    public HoldingStatus getStatus() {
        return status;
    }

    public void setStatus(HoldingStatus status) {
        this.status = status;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public String getGpsGeom() {
        return gpsGeom;
    }

    public void setGpsGeom(String gpsGeom) {
        this.gpsGeom = gpsGeom;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public boolean isSurveyed() {
        return surveyed;
    }

    public void setSurveyed(boolean surveyed) {
        this.surveyed = surveyed;
    }

    public boolean isUploaded() {
        return uploaded;
    }

    public void setUploaded(boolean uploaded) {
        this.uploaded = uploaded;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }


    public String getHoldingUid() {
        return holdingUid;
    }

    public void setHoldingUid(String holdingUid) {
        this.holdingUid = holdingUid;
    }

    public boolean isChanged() {
        return changed;
    }

    public void setChanged(boolean changed) {
        this.changed = changed;
    }

    public String getHoldingNo() {
        return holdingNo;
    }

    public void setHoldingNo(String holdingNo) {
        this.holdingNo = holdingNo;
    }

}
