package com.dosinfotech.surveyor.district;

public enum HoldingStatus {
    NOT_FOUND,GHOST,AMALGAMATED,DISPUTED,PLOTTED,ALL,NULL
}
