package com.dosinfotech.surveyor.district.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.dosinfotech.surveyor.mgm.HoldingStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import java.io.Serializable;
import java.util.Date;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(tableName = "hfa")
public class HFA implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;
    private int gid;
    @ColumnInfo(name = "holding_uid")
    private String holdingUid;
    @ColumnInfo(name = "beneficiary_code")
    private long beneficiaryCode;
    @ColumnInfo(name = "name")
    private String name;
    @ColumnInfo(name = "father_name")
    private String fatherName;
    @ColumnInfo(name = "present_address")
    private String presentAddress;
    @ColumnInfo(name = "permanent_address")
    private String permanentAddress;
    @ColumnInfo(name = "survey_code")
    private int surveyCode;
    @ColumnInfo(name = "ward")
    private int wardNo;
    @ColumnInfo(name = "status")
    private HoldingStatus status;
    @ColumnInfo(name = "image")
    private String image;
    @ColumnInfo(name = "json")
    private String json;
    @ColumnInfo(name = "gps_geom")
    private String gpsGeom;
    @ColumnInfo(name = "remarks")
    private String remarks;
    @ColumnInfo(name = "surveyed")
    private boolean surveyed=false;
    @ColumnInfo(name = "uploaded")
    private boolean uploaded=false;
    @ColumnInfo(name = "changed")
    private boolean changed=false;
    @ColumnInfo(name = "date_time")
    private Date dateTime=new Date();


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getBeneficiaryCode() {
        return beneficiaryCode;
    }

    public void setBeneficiaryCode(long beneficiaryCode) {
        this.beneficiaryCode = beneficiaryCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getPresentAddress() {
        return presentAddress;
    }

    public void setPresentAddress(String presentAddress) {
        this.presentAddress = presentAddress;
    }

    public String getPermanentAddress() {
        return permanentAddress;
    }

    public void setPermanentAddress(String permanentAddress) {
        this.permanentAddress = permanentAddress;
    }

    public int getSurveyCode() {
        return surveyCode;
    }

    public void setSurveyCode(int surveyCode) {
        this.surveyCode = surveyCode;
    }

    public int getGid() {
        return gid;
    }

    public void setGid(int gid) {
        this.gid = gid;
    }

    public int getWardNo() {
        return wardNo;
    }

    public void setWardNo(int wardNo) {
        this.wardNo = wardNo;
    }

    public String getHoldingUid() {
        return holdingUid;
    }

    public void setHoldingUid(String holdingUid) {
        this.holdingUid = holdingUid;
    }

    public HoldingStatus getStatus() {
        return status;
    }

    public void setStatus(HoldingStatus status) {
        this.status = status;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public String getGpsGeom() {
        return gpsGeom;
    }

    public void setGpsGeom(String gpsGeom) {
        this.gpsGeom = gpsGeom;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public boolean isSurveyed() {
        return surveyed;
    }

    public void setSurveyed(boolean surveyed) {
        this.surveyed = surveyed;
    }

    public boolean isUploaded() {
        return uploaded;
    }

    public void setUploaded(boolean uploaded) {
        this.uploaded = uploaded;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }


    public boolean isChanged() {
        return changed;
    }

    public void setChanged(boolean changed) {
        this.changed = changed;
    }
}
