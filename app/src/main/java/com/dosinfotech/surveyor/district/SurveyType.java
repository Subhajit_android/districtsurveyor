package com.dosinfotech.surveyor.district;

/**
 * Author        : Arpan Bose(arpanbose@hotmail.com)
 * Reviewed by   : Arpan Bose (arpanbose@hotmail.com)
 * Creation Date : 8/23/2018 2:01 PM
 * Modified on   : 8/23/2018 2:01 PM
 * Dependency    :
 * Description   :
 * Execute like  :
 **/
public enum SurveyType {
    NORMAL,HOLDING
}
