package com.dosinfotech.surveyor.district;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import com.dosinfotech.surveyor.district.http.HttpHandler;
import com.dosinfotech.surveyor.district.http.HttpResponse;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by DOS20 on 9/7/2017.
 */

public class RegistrationFragment extends Fragment implements HttpResponse {
    EditText name,mobile;
    Button register_btn;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;
    WaitingDialog wd;
    Bundle args;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedpreferences = getActivity().getSharedPreferences(AppConfig.MyPREFERENCES, Context.MODE_PRIVATE);
        editor=sharedpreferences.edit();
        wd=new WaitingDialog(getActivity(), R.style.Theme_AppCompat_Dialog);
        args=new Bundle();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_registration,container,false);
        name= (EditText) v.findViewById(R.id.name);
        mobile= (EditText) v.findViewById(R.id.mobile_no);
        register_btn= (Button) v.findViewById(R.id.register);


        register_btn.setOnClickListener(view -> {
            name.setError(null);
            mobile.setError(null);
            if (name.getText().toString().trim().isEmpty()
                    ||mobile.getText().toString().trim().isEmpty()
                    ){
                if (name.getText().toString().trim().isEmpty()){
                    name.setError(getResources().getString(R.string.name_error));
                }
                if (mobile.getText().toString().trim().isEmpty()){
                    mobile.setError(getResources().getString(R.string.mobile_error));
                }
            }else{
                if(mobile.getText().toString().trim().length()<10){
                    mobile.setError(getResources().getString(R.string.mobile_length_error));
                    return;
                }
                args.putString("msg",getResources().getString(R.string.registering));
                wd.show();
                wd.setMsgText(getResources().getString(R.string.registering));
                JSONObject main_jo = new JSONObject();
                try {
                    main_jo.put("name",name.getText().toString().trim());
                    main_jo.put("contact",mobile.getText().toString().trim());
                    main_jo.put("imei",AppConfig.getIMEI(getActivity()));
                    main_jo.put("password","123");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                HttpHandler.doPostRequest(getString(R.string.registration),main_jo,RegistrationFragment.this,getActivity());
            }
        });


        return v;
    }

    private void showAlert(String msg, final boolean finish){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.app_name));
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setPositiveButton("OK", (dialogInterface, i) -> {
            if (finish) {
                getActivity().finish();
            }
        });
        // Set other dialog properties
        builder.setTitle(getResources().getString(R.string.app_name))
                .setMessage(msg);
        // Create the AlertDialog
        builder.show();
    }

    @Override
    public void onResponse(JSONObject jsonObject) {
        wd.dismiss();
        try {
            if (jsonObject.has("module_name")){
                if (jsonObject.getString("module_name").equals("AppRegistration") && jsonObject.getBoolean("success")){
                    showAlert(getResources().getString(R.string.registation_msg),true);
                }else {
                    if(!jsonObject.getBoolean("mobile")) {
                        showAlert("Mobile no. already registered", false);
                    }else{
                        showAlert("Failed to register.", false);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(int e) {
        if (e==500){
            wd.dismiss();
        }
    }
}
