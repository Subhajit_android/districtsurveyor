package com.dosinfotech.surveyor.district;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.dosinfotech.surveyor.district.http.HttpHandler;
import com.dosinfotech.surveyor.district.http.HttpResponse;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by DOS20 on 9/7/2017.
 */

public class SplashScreenFragment extends Fragment implements HttpResponse {
    private static final int MY_PERMISSIONS_READ_PHONE_STATE = 10;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedpreferences = getActivity().getSharedPreferences(AppConfig.MyPREFERENCES, Context.MODE_PRIVATE);
        editor=sharedpreferences.edit();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_splashscreen,container,false);
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE},
                            MY_PERMISSIONS_READ_PHONE_STATE);
        }else{
            serverCheck();
        }
        return v;
    }

    private void serverCheck(){
        JSONObject jo = new JSONObject();
        try {
            jo.put("imei", AppConfig.getIMEI(getActivity()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HttpHandler.doPostRequest(getString(R.string.check_registration),jo,SplashScreenFragment.this, getActivity());
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_READ_PHONE_STATE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    serverCheck();
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    // Add the buttons
                    builder.setPositiveButton("OK", (dialogInterface, i) -> requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE},
                            MY_PERMISSIONS_READ_PHONE_STATE));
                    builder.setNegativeButton(android.R.string.no, (dialog, which) -> getActivity().finish());
                    builder.setTitle(getResources().getString(R.string.app_name))
                            .setMessage("Permission required in order to operate");
                    builder.show();
                }
                break;
        }
    }

    private void showAlert(String msg, final boolean finish){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Add the buttons
        builder.setPositiveButton("OK", (dialogInterface, i) -> {
            if (finish) {
                getActivity().finish();
            }
        });
        builder.setTitle(getResources().getString(R.string.app_name))
                .setMessage(msg);
        builder.show();
    }


    @Override
    public void onResponse(JSONObject jsonObject) {
        try {
            if(jsonObject.has("module_name")) {
                if (jsonObject.getString("module_name").equals("ServerCheck") && jsonObject.getBoolean("success")) {
                    if (jsonObject.getString("response").equals("pending")) {
                         showAlert(getResources().getString(R.string.account_under_approval), true);
                    } else if (jsonObject.getString("response").equals("suspended")) {
                        showAlert(getResources().getString(R.string.account_suspended), true);
                    } else if (jsonObject.getString("response").equals("not registered")) {

                    } else if (jsonObject.getString("response").equals("registered")) {
                        editor.putString("uid", jsonObject.getString("app_uid"));
                        editor.commit();
                           getFragmentManager()
                                   .beginTransaction()
                                   .replace(R.id.myFrame,new LoginFragment())
                                   .commitAllowingStateLoss();
                    }
                } else {
                    getFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(R.animator.enter_from_left,
                                    R.animator.enter_from_right, R.animator.exit_from_right,
                                    R.animator.exit_from_left)
                            .replace(R.id.myFrame, new RegistrationFragment())
                            .commitAllowingStateLoss();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(int e) {
//        if(e==403){
//            showAlert("Authentication error, contact vendor", true);
//        }
//        if (e==500){
//            showAlert("Server encountered an error.", true);
//        }
    }


}
