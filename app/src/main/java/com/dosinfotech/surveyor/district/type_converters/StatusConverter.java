package com.dosinfotech.surveyor.district.type_converters;

import androidx.room.TypeConverter;

import com.dosinfotech.surveyor.district.HoldingStatus;

public class StatusConverter {
    @TypeConverter
    public static HoldingStatus toStatus(String status) {
        if (status!=null) {
            return HoldingStatus.valueOf(status);
        }else{
            return HoldingStatus.NULL;
        }
    }

    @TypeConverter
    public static String toString(HoldingStatus holdingStatus) {
        if(holdingStatus!=null) {
            return holdingStatus.toString();
        }else{
            return null;
        }
    }
}
