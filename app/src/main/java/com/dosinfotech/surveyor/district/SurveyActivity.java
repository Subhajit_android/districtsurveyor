package com.dosinfotech.surveyor.district;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

public class SurveyActivity extends AppCompatActivity {

    String subtitle="";
    SurveyType surveyType=null;
    HoldingSurveyType holdingSurveyType=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        surveyType=SurveyType.valueOf(getIntent().getStringExtra("survey_type"));
        Log.v("SURVEYOR_LOG",surveyType.toString());

        getSupportFragmentManager().addOnBackStackChangedListener(() -> {
        if (getSupportFragmentManager().getBackStackEntryCount()>1) {
            getSupportActionBar().setHomeButtonEnabled(true);
        }else{
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setSubtitle(null);
        }
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.myFrame);
            if (currentFragment instanceof SurveyListFragment){
                if (!getSupportActionBar().isShowing()) {
                    getSupportActionBar().show();
                }
            }
        });

        Fragment myFragment=new SurveyListFragment();
        Bundle args=new Bundle();
        args.putString("survey_type",surveyType.toString());

        if(getIntent().hasExtra("holding_type")){
            holdingSurveyType=HoldingSurveyType.valueOf(getIntent().getStringExtra("holding_type"));
            args.putString("holding_type",holdingSurveyType.toString());
        }

        if (surveyType==SurveyType.NORMAL){
            subtitle=getResources().getString(R.string.normal);
        }else{
            if(holdingSurveyType==HoldingSurveyType.HOUSE_HOLD){
                myFragment=new HoldingListFragment();
            }else {
                myFragment=new BsupHfaIihlListFragment();
            }
            args.putString("holding_type",holdingSurveyType.toString());
        }

        myFragment.setArguments(args);
        getSupportFragmentManager()
                .beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .add(R.id.myFrame,myFragment)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }


 /*   @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.myFrame);
        if (currentFragment instanceof HomeFramgment){
            getSupportActionBar().setSubtitle(null);
        }else if (currentFragment instanceof HomeFramgment
                ||currentFragment instanceof MapService
                ||currentFragment instanceof MapService
                ||currentFragment instanceof ShopList
                ||currentFragment instanceof BsupHfaIihlListFragment
                ||currentFragment instanceof MultipleHoldingList
                ||currentFragment instanceof FormFragmant) {
            currentFragment.onActivityResult(requestCode,resultCode,data);
        }
    }*/


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.myFrame);
            if (getSupportFragmentManager().getBackStackEntryCount()>1) {
               /* if (currentFragment instanceof FormFragmant
                        || currentFragment instanceof MultipleHoldingList
                        || currentFragment instanceof RemarkFragment
                        || currentFragment instanceof ShopList) {
                    ConfirmExit();
                    return false;
                }*/
                getSupportFragmentManager().popBackStackImmediate();
            }else{
                finish();
            }
        }
        return super.onOptionsItemSelected(item);
    }

  /*
    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount()>1) {
            Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.myFrame);
            if (currentFragment instanceof FormFragmant
                    ||currentFragment instanceof MultipleHoldingList
                    || currentFragment instanceof RemarkFragment
                    || currentFragment instanceof ShopList) {
                ConfirmExit();
                return;
            }
            getSupportFragmentManager().popBackStackImmediate();
        }else {
            finish();
        }
    }*/

    /**
     * confirmation before exit
     */
    private void ConfirmExit() {
        new AlertDialog.Builder(this)
                .setMessage("Sure to exit?")
                .setPositiveButton("Yes", (dialogInterface, i) -> getSupportFragmentManager().popBackStackImmediate())
                .setNegativeButton("No", null)
                .show();
    }
}
