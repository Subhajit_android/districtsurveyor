package com.dosinfotech.surveyor.district.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.dosinfotech.surveyor.mgm.HoldingStatus;

import java.io.Serializable;
import java.util.Date;

@Entity(tableName = "holding")
public class Holding implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;
    @ColumnInfo(name = "uid")
    private String uid;
    @ColumnInfo(name = "holding_no")
    private String holdingNo;
    @ColumnInfo(name = "assessee")
    private String assessee;
    @ColumnInfo(name = "street_name")
    private String streetName;
    @ColumnInfo(name = "street_code")
    private String streetCode;
    @ColumnInfo(name = "ward")
    private int ward;
    @ColumnInfo(name = "status")
    private HoldingStatus status=HoldingStatus.NULL;
    @ColumnInfo(name = "image")
    private String image=null;
    @ColumnInfo(name = "json")
    private String json=null;
    @ColumnInfo(name = "gps_geom")
    private String gpsGeom=null;
    @ColumnInfo(name = "remarks")
    private String remarks=null;
    @ColumnInfo(name = "surveyed")
    private boolean surveyed=false;
    @ColumnInfo(name = "uploaded")
    private boolean uploaded=false;
    @ColumnInfo(name = "previously_surveyed")
    private boolean previouslySurveyed=false;
    @ColumnInfo(name = "changed")
    private boolean changed=false;
    @ColumnInfo(name = "date_time")
    private Date dateTime=new Date();

    @Ignore
    private boolean lockData;

    public Holding() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getHoldingNo() {
        return holdingNo;
    }

    public void setHoldingNo(String holdingNo) {
        this.holdingNo = holdingNo;
    }

    public String getAssessee() {
        return assessee;
    }

    public void setAssessee(String assessee) {
        this.assessee = assessee;
    }

    public String getStreetCode() {
        return streetCode;
    }

    public void setStreetCode(String streetCode) {
        this.streetCode = streetCode;
    }

    public int getWard() {
        return ward;
    }

    public void setWard(int ward) {
        this.ward = ward;
    }

    public HoldingStatus getStatus() {
        return status;
    }

    public void setStatus(HoldingStatus status) {
        this.status = status;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public String getGpsGeom() {
        return gpsGeom;
    }

    public void setGpsGeom(String gpsGeom) {
        this.gpsGeom = gpsGeom;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public boolean isSurveyed() {
        return surveyed;
    }

    public void setSurveyed(boolean surveyed) {
        this.surveyed = surveyed;
    }

    public boolean isUploaded() {
        return uploaded;
    }

    public void setUploaded(boolean uploaded) {
        this.uploaded = uploaded;
    }

    public boolean isPreviouslySurveyed() {
        return previouslySurveyed;
    }

    public void setPreviouslySurveyed(boolean previouslySurveyed) {
        this.previouslySurveyed = previouslySurveyed;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public boolean isLockData() {
        return lockData;
    }

    public void setLockData(boolean lockData) {
        this.lockData = lockData;
    }

    public boolean isChanged() {
        return changed;
    }

    public void setChanged(boolean changed) {
        this.changed = changed;
    }

}
