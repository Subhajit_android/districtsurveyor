package com.dosinfotech.surveyor.district.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.dosinfotech.surveyor.district.HoldingStatus;
import com.dosinfotech.surveyor.district.entity.BSUP;
import com.dosinfotech.surveyor.district.entity.IIHL;

import java.util.List;

@Dao
public interface IIHLDao {

   /* @Insert
    long[] insertAllData(List<IIHL> IIHLList);

    @Query("SELECT * FROM bsup WHERE ward=:ward")
    List<BSUP> findAllDataByWard(int ward);

    @Query("SELECT COUNT(id) FROM iihl where ward=:ward")
    long countData(int ward);

    @Query("SELECT * FROM iihl WHERE surveyed=:surveyed AND ward=:ward ORDER BY id DESC")
    List<IIHL> findAllIIHLByWardAndSurveyed(int ward, boolean surveyed);

    @Query("SELECT * FROM iihl WHERE status=:status AND surveyed=:surveyed AND ward=:ward AND uploaded=0 ORDER BY id DESC")
    List<IIHL> findAllIIHLByWardAndSurveyedAndStatus(int ward, boolean surveyed, HoldingStatus status);

    @Query("SELECT * FROM iihl WHERE gid IN (:gid)")
    List<IIHL> findAllByAllGid(List<String> gid);

    @Update
    int updateIIHL(IIHL iihl);

    @Query("SELECT * FROM iihl WHERE gid=:gid")
    IIHL findIIHLByGid(int gid);

    @Query("SELECT * FROM iihl WHERE gid IN (:ids) AND uploaded=0")
    List<IIHL> findIIHLForUpload(List<String> ids);

    @Query("UPDATE iihl SET uploaded=1 WHERE gid IN (:ids)")
    int updateOnUploadSuccess(List<String> ids);

    @Query("SELECT * FROM iihl WHERE surveyed=:surveyed and uploaded=:uploaded and ward=:ward")
    List<IIHL> findIIHLBywardAndUploaded(boolean surveyed, boolean uploaded, int ward);

    @Query("delete FROM iihl WHERE ward=:ward")
    void removeData(int ward);*/
}
