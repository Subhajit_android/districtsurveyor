package com.dosinfotech.surveyor.district;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.FrameLayout;

public class Activity1 extends AppCompatActivity {

    FrameLayout fl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_1);
        fl=findViewById(R.id.myFrame);
        fl.setBackground(getResources().getDrawable(R.drawable.bg));
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.myFrame,new SplashScreenFragment())
                .commitAllowingStateLoss();
    }


    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.myFrame);
        if(currentFragment instanceof Dashboard){
            currentFragment.onActivityResult(requestCode,resultCode,data);
        }

    }*/

}
