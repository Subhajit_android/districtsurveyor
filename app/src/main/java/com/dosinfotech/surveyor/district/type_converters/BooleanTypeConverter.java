package com.dosinfotech.surveyor.district.type_converters;

import androidx.room.TypeConverter;

public class BooleanTypeConverter {
    @TypeConverter
    public static boolean toBoolean(int value) {
        return value == 0 ? false : true;
    }

    @TypeConverter
    public static int toInt(boolean value) {
        return value == true ? 1 : 0;
    }
}
