package com.dosinfotech.surveyor.district.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.dosinfotech.surveyor.district.HoldingStatus;
import com.dosinfotech.surveyor.district.entity.Holding;

import java.util.List;

@Dao
public interface HoldingDao {
  /*  @Insert(onConflict = OnConflictStrategy.REPLACE)
    long[] insertAllData(List<Holding> holdingList);

    @Query("SELECT * FROM holding WHERE surveyed=0")
    List<Holding> findAllData();

    @Query("SELECT * FROM holding WHERE ward=:ward")
    List<Holding> findAllHoldingByWard(int ward);

    @Query("SELECT COUNT(id) FROM holding WHERE ward=:ward")
    long countHoldingByWard(int ward);

    @Query("SELECT * FROM holding WHERE uid IN (:ids) AND uploaded=0")
    List<Holding> findHoldingsForUpload(List<String> ids);

    @Query("SELECT * FROM holding WHERE uid IN (:uids)")
    List<Holding> findHoldingsByListHoldingUID(List<String> uids);

    @Query("UPDATE holding SET uploaded=1 WHERE uid IN (:ids)")
    int updateOnUploadSuccess(List<String> ids);

    @Update
    int updateHolding(Holding holding);

    @Update
    int updateMultipleHolding(List<Holding> holdings);

    @Query("SELECT * FROM holding WHERE uid IN (:uids)")
    List<Holding> findMultipleHoldingsByUid(List<String> uids);

    @Query("SELECT * FROM holding WHERE uid=:uid")
    Holding findHoldingByUID(String uid);

    @Query("SELECT * FROM holding WHERE status=:status AND surveyed=:surveyed AND ward=:ward AND previously_surveyed=:preSurvey AND uploaded=0 ORDER BY date(date_time) DESC")
    List<Holding> findHoldingsByWardAndSurveyedAndStatusAndPreSurvey(int ward, boolean surveyed, HoldingStatus status, boolean preSurvey);

    @Query("SELECT * FROM holding WHERE surveyed=:surveyed AND ward=:ward AND previously_surveyed=:preSurvey ORDER BY date(date_time) DESC")
    List<Holding> findHoldingsByWardAndSurveyedAndPreSurvey(int ward, boolean surveyed, boolean preSurvey);
    @Query("Select DISTINCT street_code from holding")
    List<String> findAllDistinctStreetCodes();

    @Query("SELECT * FROM holding WHERE surveyed=:surveyed and uploaded=:uploaded and ward=:ward")
    List<Holding> findHoldingsBywardAndUploaded(boolean surveyed, boolean uploaded, int ward);

    @Query("delete FROM holding WHERE ward=:ward")
    void removeData(int ward);*/

}
