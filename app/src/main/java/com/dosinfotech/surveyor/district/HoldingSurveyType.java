package com.dosinfotech.surveyor.district;

/**
 * Author        : Arpan Bose(arpanbose@hotmail.com)
 * Reviewed by   : Arpan Bose (arpanbose@hotmail.com)
 * Creation Date : 8/22/2018 11:48 AM
 * Modified on   : 8/22/2018 11:48 AM
 * Dependency    :
 * Description   :
 * Execute like  :
 **/
public enum HoldingSurveyType {
    HOUSE_HOLD,SHOP,HOUSING_FOR_ALL,BSUP,IIHL,LAYER_HOLDING_TAGGING
}
