package com.dosinfotech.surveyor.district.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.dosinfotech.surveyor.district.AppConfig;
import com.dosinfotech.surveyor.district.dao.BSUPDao;
import com.dosinfotech.surveyor.district.dao.HFADao;
import com.dosinfotech.surveyor.district.dao.HoldingDao;
import com.dosinfotech.surveyor.district.dao.IIHLDao;
import com.dosinfotech.surveyor.district.dao.PreferenceDao;
import com.dosinfotech.surveyor.district.dao.ShopDao;
import com.dosinfotech.surveyor.district.entity.BSUP;
import com.dosinfotech.surveyor.district.entity.HFA;
import com.dosinfotech.surveyor.district.entity.Holding;
import com.dosinfotech.surveyor.district.entity.IIHL;
import com.dosinfotech.surveyor.district.entity.Preference;
import com.dosinfotech.surveyor.district.entity.Shop;
import com.dosinfotech.surveyor.district.type_converters.BooleanTypeConverter;
import com.dosinfotech.surveyor.district.type_converters.DateTypeConverter;
import com.dosinfotech.surveyor.district.type_converters.StatusConverter;


@Database(entities = {Holding.class, Shop.class, BSUP.class, IIHL.class, HFA.class, Preference.class}, version = AppConfig.DATABASE_VERSION,exportSchema = false)
@TypeConverters({DateTypeConverter.class, BooleanTypeConverter.class, StatusConverter.class})
public abstract class AppDatabase extends RoomDatabase {

    public abstract HoldingDao holdingDao();
    public abstract ShopDao shopDao();
    public abstract BSUPDao bsupDao();
    public abstract HFADao hfaDao();
    public abstract IIHLDao iihlDao();
    public abstract PreferenceDao preferenceDao();


}
