package com.dosinfotech.surveyor.district.http;

import org.json.JSONObject;

/**
 * Created by LORD on 1/26/2018.
 */

public interface HttpResponse {
    void onResponse(JSONObject jsonObject);
    void onFailure(int errorCode);
}
