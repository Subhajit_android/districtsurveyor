package com.dosinfotech.surveyor.district;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.dosinfotech.surveyor.district.http.HttpHandler;
import com.dosinfotech.surveyor.district.http.HttpResponse;
import org.json.JSONException;
import org.json.JSONObject;

public class LoginFragment extends Fragment implements HttpResponse {

    EditText userId,password;
    Button login_btn;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;
    WaitingDialog wd;
    Bundle args;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedpreferences = getActivity().getSharedPreferences(AppConfig.MyPREFERENCES, Context.MODE_PRIVATE);
        editor=sharedpreferences.edit();
        wd=new WaitingDialog(getActivity(), R.style.Theme_AppCompat_Dialog);
        args=new Bundle();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.fragment_login,container,false);
        userId= (EditText) v.findViewById(R.id.userId);
        password= (EditText) v.findViewById(R.id.password);
        login_btn= (Button) v.findViewById(R.id.login);

        login_btn.setOnClickListener(view -> {
            /*userId.setError(null);
            password.setError(null);
            if (userId.getText().toString().trim().isEmpty()
                    ||password.getText().toString().trim().isEmpty()){
            }else{
                wd.show();
                wd.setMsgText(getResources().getString(R.string.registering));
                JSONObject main_jo = new JSONObject();
                try {
                    main_jo.put("name",userId.getText().toString().trim());
                    main_jo.put("contact",password.getText().toString().trim());
                    main_jo.put("imei",AppConfig.getIMEI(getActivity()));
                    main_jo.put("password","123");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                HttpHandler.doPostRequest(getString(R.string.login),main_jo,LoginFragment.this,getActivity());
            }*/

            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.myFrame,new Dashboard())
                    .commitAllowingStateLoss();

        });


        return v;
    }

    private void showAlert(String msg, final boolean finish){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.app_name));
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setPositiveButton("OK", (dialogInterface, i) -> {
            if (finish) {
                getActivity().finish();
            }
        });
        // Set other dialog properties
        builder.setTitle(getResources().getString(R.string.app_name))
                .setMessage(msg);
        // Create the AlertDialog
        builder.show();
    }
    @Override
    public void onResponse(JSONObject jsonObject) {
        wd.dismiss();
        try {
            if (jsonObject.has("module_name")){
                if (jsonObject.getString("module_name").equals("AppLogin") && jsonObject.getBoolean("success")){
                    editor.putString("uid", jsonObject.getString("app_uid"));
                    editor.commit();
                    getFragmentManager()
                            .beginTransaction()
                            .replace(R.id.myFrame,new Dashboard())
                            .commitAllowingStateLoss();
                }else {
                    showAlert("Unsuccessful Login ", false);

                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(int errorCode) {
        if (errorCode==500){
            wd.dismiss();
        }
    }
}
