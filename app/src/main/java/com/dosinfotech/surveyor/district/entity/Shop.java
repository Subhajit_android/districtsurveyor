package com.dosinfotech.surveyor.district.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.dosinfotech.surveyor.district.HoldingStatus;

import java.io.Serializable;
import java.util.Date;

@Entity(tableName = "shop")
public class Shop implements Serializable {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;
    @ColumnInfo(name = "holding_uid")
    private String holdingUid;

    @ColumnInfo(name = "holding_no")
    private String holdingNo;
    @ColumnInfo(name = "name")
    private String name;
    @ColumnInfo(name = "proprietor_name")
    private String proprietorName;
    @ColumnInfo(name = "lic_no")
    private String licNo;
    @ColumnInfo(name = "ward")
    private int ward;
    @ColumnInfo(name = "holding_status")
    private HoldingStatus holdingStatus;
    @ColumnInfo(name = "status")
    private HoldingStatus status;
    @ColumnInfo(name = "image")
    private String image;
    @ColumnInfo(name = "json")
    private String json;
    @ColumnInfo(name = "gps_geom")
    private String gpsGeom;
    @ColumnInfo(name = "remarks")
    private String remarks;
    @ColumnInfo(name = "surveyed")
    private boolean surveyed=false;
    @ColumnInfo(name = "uploaded")
    private boolean uploaded=false;
    @ColumnInfo(name = "new_survey")
    private boolean newSurvey=false;
    @ColumnInfo(name = "changed")
    private boolean changed=false;
    @ColumnInfo(name = "date_time")
    private Date dateTime=new Date();

    @Ignore
    private boolean lockData;

    public Shop() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHoldingUid() {
        return holdingUid;
    }

    public void setHoldingUid(String holdingUid) {
        this.holdingUid = holdingUid;
    }

    public String getHoldingNo() {
        return holdingNo;
    }

    public void setHoldingNo(String holdingNo) {
        this.holdingNo = holdingNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLicNo() {
        return licNo;
    }

    public void setLicNo(String licNo) {
        this.licNo = licNo;
    }

    public int getWard() {
        return ward;
    }

    public void setWard(int ward) {
        this.ward = ward;
    }

    public HoldingStatus getHoldingStatus() {
        return holdingStatus;
    }

    public void setHoldingStatus(HoldingStatus holdingStatus) {
        this.holdingStatus = holdingStatus;
    }

    public HoldingStatus getStatus() {
        return status;
    }

    public void setStatus(HoldingStatus status) {
        this.status = status;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public String getGpsGeom() {
        return gpsGeom;
    }

    public void setGpsGeom(String gpsGeom) {
        this.gpsGeom = gpsGeom;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public boolean isSurveyed() {
        return surveyed;
    }

    public void setSurveyed(boolean surveyed) {
        this.surveyed = surveyed;
    }

    public boolean isUploaded() {
        return uploaded;
    }

    public void setUploaded(boolean uploaded) {
        this.uploaded = uploaded;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public String getProprietorName() {
        return proprietorName;
    }

    public void setProprietorName(String proprietorName) {
        this.proprietorName = proprietorName;
    }

    public boolean isLockData() {
        return lockData;
    }

    public void setLockData(boolean lockData) {
        this.lockData = lockData;
    }

    public boolean isNewSurvey() {
        return newSurvey;
    }

    public void setNewSurvey(boolean newSurvey) {
        this.newSurvey = newSurvey;
    }

    public boolean isChanged() {
        return changed;
    }

    public void setChanged(boolean changed) {
        this.changed = changed;
    }

}
