package com.dosinfotech.surveyor.district.http;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import androidx.appcompat.app.AlertDialog;

import com.dosinfotech.surveyor.district.GetShaFingerPrint;
import com.dosinfotech.surveyor.district.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by LORD on 1/26/2018.
 */

public class HttpHandler {
    private static final String TAG ="SURVEYOR_HTTP_LOG";
    private static OkHttpClient client = new OkHttpClient();
    private static final MediaType JSON= MediaType.parse("application/json");

    private static int connectionTimeOut=20000,readTimeOut=20000;

    public static void doPostRequest(String url, JSONObject jsonObject, final HttpResponse httpResponse, final Context context){
        try {
            client = client.newBuilder()
                    .connectTimeout(connectionTimeOut, TimeUnit.MILLISECONDS)
                    .readTimeout(readTimeOut, TimeUnit.MILLISECONDS)
                    .build();
            RequestBody body = RequestBody.create(JSON, jsonObject.toString());
            Log.v(TAG, jsonObject.toString() + "->" + url);
            Request request = new Request.Builder()
                    .url(url)
                    .addHeader("sha-key", new GetShaFingerPrint(context).getSha1())
                    .addHeader("Content-Type", "application/json")
                    .post(body)
                    .build();
            client.newCall(request)
                    .enqueue(new Callback() {
                        @Override
                        public void onFailure(final Call call, IOException e) {
                            if (context instanceof Activity) {
                                ((Activity) context).runOnUiThread(() -> {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                    builder.setTitle(context.getString(R.string.app_name));
                                    builder.setIcon(R.mipmap.ic_launcher);
                                    builder.setCancelable(false);
                                    builder.setPositiveButton("Retry", (dialogInterface, i) -> {
                                        call.clone().enqueue(this);
                                    });
                                    builder.setNegativeButton("Cancel", (dialog, which) -> ((Activity) context).finish());
                                    // Set other dialog properties
                                    builder.setTitle(context.getResources().getString(R.string.app_name))
                                            .setMessage("Couldn't get any response from server.");
                                    builder.show();
                                });
                            } else {
                                Log.v(TAG, "Couldn't get any response from server.");
                            }
                        }

                        @Override
                        public void onResponse(Call call, final Response response) throws IOException {
                            final String res = response.body().string();
                            final int code = response.code();
                            Log.v(TAG, code + "->" + res);
                            if (context instanceof Activity) {
                                ((Activity) context).runOnUiThread(() -> {
                                    String title=null;
                                    String msg=null;
                                    switch (code) {
                                        case 200:
                                            try {
                                                httpResponse.onResponse(new JSONObject(res));
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            break;
                                        case 500:
                                            title="Internal Server error";
                                            msg="There is a temporary issue with the server, please try again later.";
                                            break;
                                        case 404:
                                            title="Page Not Found";
                                            msg="The requested destination is not available.";
                                            break;
                                        case 403:
                                            title="Forbidden";
                                            msg="you don’t have permission to access.";
                                            break;
                                        case 401:
                                            title="Unauthorised";
                                            msg="You have attempted to access a page for which you are not authorized.";
                                            break;
                                        case 400:
                                            title="Bad Request";
                                            msg="You sent a request that this server could not understand.";

                                            break;
                                    }
                                    if (code != 200) {
                                        httpResponse.onFailure(code);
                                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                        builder.setTitle(title);
                                        builder.setIcon(R.mipmap.ic_launcher);
                                        builder.setMessage(msg);
                                        builder.setPositiveButton("OK", null);
                                        try {
                                            builder.show();
                                        }catch (Exception e){
                                            e.printStackTrace();
                                        }
                                    }
                                });
                            } else {
                                if (code != 200) {
                                    httpResponse.onFailure(code);
                                } else {
                                    try {
                                        httpResponse.onResponse(new JSONObject(res));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    });
        }catch (Exception e){
            httpResponse.onFailure(700);
        }
    }

    public static int getConnectionTimeOut() {
        return connectionTimeOut;
    }

    public static void setConnectionTimeOut(int connectionTimeOut) {
        HttpHandler.connectionTimeOut = connectionTimeOut;
    }

    public static int getReadTimeOut() {
        return readTimeOut;
    }

    public static void setReadTimeOut(int readTimeOut) {
        HttpHandler.readTimeOut = readTimeOut;
    }
}
