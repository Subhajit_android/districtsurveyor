package com.dosinfotech.surveyor.district.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.dosinfotech.surveyor.district.HoldingStatus;
import com.dosinfotech.surveyor.district.entity.BSUP;
import com.dosinfotech.surveyor.district.entity.HFA;

import java.util.List;

@Dao
public interface HFADao {
   /* @Insert
    long[] insertAllData(List<HFA> hfaList);

    @Query("SELECT * FROM bsup WHERE ward=:ward")
    List<BSUP> findAllDataByWard(int ward);

    @Query("SELECT COUNT(id) FROM hfa where ward=:ward")
    long countData(int ward);

    @Query("SELECT * FROM hfa WHERE surveyed=:surveyed AND ward=:ward ORDER BY id DESC")
    List<HFA> findAllHFAByWardAndSurveyed(int ward, boolean surveyed);

    @Query("SELECT * FROM hfa WHERE status=:status AND surveyed=:surveyed AND ward=:ward AND uploaded=0 ORDER BY id DESC")
    List<HFA> findAllHFAByWardAndSurveyedAndStatus(int ward, boolean surveyed, HoldingStatus status);

    @Query("SELECT * FROM hfa WHERE gid IN (:gid)")
    List<HFA> findAllByAllGid(List<String> gid);

    @Update
    int updateHFA(HFA hfa);

    @Query("SELECT * FROM hfa WHERE gid=:gid")
    HFA findHFAByGid(int gid);

    @Query("SELECT * FROM hfa WHERE gid IN (:ids) AND uploaded=0")
    List<HFA> findHFAForUpload(List<String> ids);

    @Query("UPDATE hfa SET uploaded=1 WHERE gid IN (:ids)")
    int updateOnUploadSuccess(List<String> ids);

    @Query("SELECT * FROM hfa WHERE surveyed=:surveyed and uploaded=:uploaded and ward=:ward")
    List<HFA> findHFABywardAndUploaded(boolean surveyed, boolean uploaded, int ward);

    @Query("delete FROM hfa WHERE ward=:ward")
    void removeData(int ward);*/
}
