package com.dosinfotech.surveyor.district;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;
import androidx.annotation.NonNull;
import com.airbnb.lottie.LottieAnimationView;

/**
 * Author        : Arpan Bose(arpanbose@hotmail.com)
 * Reviewed by   : Arpan Bose (arpanbose@hotmail.com)
 * Creation Date : 2/22/2018 1:00 PM
 * Modified on   : 2/22/2018 1:00 PM
 * Dependency    :
 * Description   :
 * Execute like  :
 **/
public class WaitingDialog extends Dialog {

    private LottieAnimationView animationView;
    private TextView msgText;

    public WaitingDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        setContentView(R.layout.wating_dialog);
        setCancelable(false);
        animationView = findViewById(R.id.lottieAnimationView);
        msgText=findViewById(R.id.text_msg);
        animationView.setAnimation("loader_ring.json");
        animationView.loop(true);
        animationView.playAnimation();
    }

    public void setMsgText(String msg){
        msgText.setText(msg);
    }

}
