package com.dosinfotech.surveyor.district;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.FileProvider;

/*import com.dosinfotech.surveyor.mgm.entity.BSUP;
import com.dosinfotech.surveyor.mgm.entity.HFA;
import com.dosinfotech.surveyor.mgm.entity.Holding;
import com.dosinfotech.surveyor.mgm.entity.IIHL;
import com.dosinfotech.surveyor.mgm.entity.Shop;*/



import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Author        : Arpan Bose(arpanbose@hotmail.com)
 * Reviewed by   : Arpan Bose(arpanbose@hotmail.com)
 * Creation Date : 7th Sep 2017
 * Modified on   : 7th Sep 2017
 * Dependency    : NA
 * Description   : Application class
 * Execute like  : NA
 **/
public class AppConfig extends Application {
    public static final int PICK_LOCATION = 36;
    public static final int REQUEST_IMAGE_CAPTURE = 37;
    public static final int REQUEST_CODE_RECOVER_PLAY_SERVICES = 87;
    public static final String DATABASE_NAME = "surveyor_holding_db";
    public static final int DATABASE_VERSION = 11;
    private SharedPreferences sharedpreferences;

    public static final String MyPREFERENCES="surveyor_mgm";
    public static final String APPLICATION_FOLDER= Environment.getExternalStorageDirectory()+ File.separator+MyPREFERENCES;
    public static final String IMAGES_FOLDER=APPLICATION_FOLDER+ File.separator+"images";

    public static final String WAITING_DIALOG_TAG = "surveyor_wait";

    public static final String MAP_CAMERA_LAT="camera_lat";
    public static final String MAP_CAMERA_LNG="camera_lng";
    public static final String MAP_CAMERA_ZOOM="camera_zoom";

    public static final String WARD_BOUNDARY_LAYER="ward_boundary";
    public static final String ROAD_LAYER="road";
    public static final String MAJOR_ROAD_LAYER="mejor_road";



    @Override
    public void onCreate() {
        super.onCreate();
        //setSurveyPreference(this);
    }

    /**
     * Function to get the IMEI number
     * @return
     */
    @SuppressLint("MissingPermission")
    public static String getIMEI(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        String imei=telephonyManager.getDeviceId();
        if(imei!=null){
            return imei;
        }else {
            return "na";
        }
    }


    /**
     * Set survey preference
     * @param context
     */
  /*  public static void setSurveyPreference(Context context) {
        final SharedPreferences mySharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        if (!mySharedPreferences.contains(context.getResources().getString(R.string.view_survey_linestring_color_key))) {
            final SharedPreferences.Editor editor = mySharedPreferences.edit();
            editor.putString(context.getResources().getString(R.string.view_survey_linestring_width_key), "5");
            editor.putInt(context.getResources().getString(R.string.view_survey_linestring_color_key), 0xff39ff32);
            editor.putString(context.getResources().getString(R.string.view_survey_polygon_strock_width_key), "5");
            editor.putInt(context.getResources().getString(R.string.view_survey_polygon_stroke_color_key), 0xff39ff32);
            editor.putInt(context.getResources().getString(R.string.view_survey_polygon_fill_color_key), 0x39ff32);
            editor.putBoolean("point_confirm", true);
            editor.commit();
        }
    }*/


    /**
     * Convert string to date
     * @param input
     * @param inputPattern
     * @return
     */
    public static Date convertToDate(String input, String inputPattern) {
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        Date date = null;
        try {
            date = inputFormat.parse(input);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    /**
     * Date to string converter
     * @param input
     * @param outPattern
     * @return
     */
    public static String dateToString(Date input, String outPattern){
        SimpleDateFormat outputFormat = new SimpleDateFormat(outPattern);
        return outputFormat.format(input);
    }


    /**
     * Alert dialog for all common purpose
     * @param finish
     * @param msg
     * @param activity
     */
    public static void showCommonAlertForFragments(final boolean finish, final String msg, final Activity activity) {
        activity.runOnUiThread(() -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            // Add the buttons
            builder.setTitle(activity.getString(R.string.app_name));
            builder.setIcon(R.mipmap.ic_launcher);
            builder.setPositiveButton("OK", (dialogInterface, i) -> {
                if (finish) {
                    activity.getFragmentManager().popBackStackImmediate();
                }
            });
            // Set other dialog properties
            builder.setTitle(activity.getResources().getString(R.string.app_name))
                    .setMessage(msg);
            // Create the AlertDialog
            builder.show();
        });

    }

    /**
     * Function to create post body for http request
     * @param sharedPreferences
     * @param context
     * @return
     */
    public static JSONObject postBody(SharedPreferences sharedPreferences, Context context){
        String uid=sharedPreferences.getString("uid","");
        JSONObject new_jo=new JSONObject();
        try {
            new_jo.put("imei",AppConfig.getIMEI(context));
            new_jo.put("app_uid",uid);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return new_jo;
    }

    /**
     * Function to create json as file
     * @param mJsonResponse
     * @param context
     * @return
     * @throws IOException
     */
    public static Uri createJsonFile(String mJsonResponse, Context context) throws IOException {
        File tempFile = new File(context.getExternalCacheDir(),context.getString(R.string.app_name)+"_"+new SimpleDateFormat("yyMMddHHmmss").format(new Date())+".geojson");
        try {
            FileWriter file = null;
            try {
                file = new FileWriter(tempFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
            file.write(mJsonResponse);
            file.flush();
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Uri.parse("file://"+tempFile.getPath());
    }

    public static int getColorWithAlpha(int color, float ratio) {
        int newColor = 0;
        int alpha = Math.round(Color.alpha(color) * ratio);
        int r = Color.red(color);
        int g = Color.green(color);
        int b = Color.blue(color);
        newColor = Color.argb(alpha, r, g, b);
        return newColor;
    }

  /*  /**
     * Filter for adapter for BSUP
     * @param bsupList all list
     * @param query
     * @return
     */
  /*  public static List<BSUP> bsupFilter(List<BSUP> bsupList, String query) {
        query = query.toLowerCase();
        final List<BSUP> filteredModelList = new ArrayList<>();
        for (BSUP bsup : bsupList) {
            final String name = bsup.getName().toLowerCase();
            final String slumName = bsup.getSlumName().toLowerCase();
            final String holdingNo = bsup.getHoldingNo();
            if ((name!=null && name.contains(query))
                    || (slumName!=null && slumName.startsWith(query))
                    || (holdingNo!=null && holdingNo.startsWith(query))) {
                filteredModelList.add(bsup);
            }
        }
        return filteredModelList;
    }

    /**
     * Filter for adapter for IIHL
     * @param iihlList all list
     * @param query
     * @return
     */
  /*  public static List<IIHL> iihlFilter(List<IIHL> iihlList, String query) {
        query = query.toLowerCase();
        final List<IIHL> filteredModelList = new ArrayList<>();
        for (IIHL iihl : iihlList) {
            //String beneficiary="",mobile="";
            String beneficiary = null,mobile=null;

            if(iihl.getBeneficiary()!=null) {
                beneficiary = iihl.getBeneficiary().toLowerCase();
            }
            if(iihl.getMobile()!=null && !iihl.getMobile().isEmpty()) {
                mobile = iihl.getMobile().toLowerCase();
            }
            if ((null!=beneficiary && beneficiary.contains(query)) || (null!=mobile && mobile.startsWith(query))) {
                filteredModelList.add(iihl);
            }
        }
        return filteredModelList;
    }

    /**
     * Filter for adapter for HFA
     * @param hfaList
     * @param query
     * @return
     */
 /*   public static List<HFA> hfaFilter(List<HFA> hfaList, String query) {
        query = query.toLowerCase();
        final List<HFA> filteredModelList = new ArrayList<>();
        for (HFA hfa : hfaList) {
            String name="",fatherName="",surveyCode="";
            if(hfa.getName()!=null && !hfa.getName().isEmpty()) {
                name = hfa.getName().toLowerCase();
            }
            if(hfa.getFatherName()!=null && !hfa.getFatherName().isEmpty()) {
                fatherName = hfa.getFatherName().toLowerCase();
            }
            if(hfa.getSurveyCode()!=0) {
                surveyCode= String.valueOf(hfa.getSurveyCode()).toLowerCase();
            }

            if (name.contains(query) || fatherName.startsWith(query)|| surveyCode.startsWith(query)) {
                filteredModelList.add(hfa);
            }
        }
        return filteredModelList;
    }

    /**
     * Fiter for adapter for House hold
     * @param houseHoldingList
     * @param query
     * @return
     */

  /*  public static List<Holding> houseHoldFilter(List<Holding> houseHoldingList, String query) {
        query = query.toLowerCase();
        final List<Holding>
                filteredModelList = new ArrayList<>();
        for (Holding holding : houseHoldingList) {
            String assesse="",holdingNo="",holdingId="";
            if(holding.getAssessee()!=null && !holding.getAssessee().isEmpty()) {
                assesse = holding.getAssessee().toLowerCase();
            }
            if(holding.getHoldingNo()!=null && !holding.getHoldingNo().isEmpty()) {
                holdingNo = holding.getHoldingNo().toLowerCase();
            }
            if(holding.getUid()!=null && !holding.getUid().isEmpty()) {
                holdingId = String.valueOf(holding.getUid()).toLowerCase();
            }

            if (assesse.contains(query) || holdingNo.startsWith(query) || holdingId.startsWith(query)) {
                filteredModelList.add(holding);
            }
        }
        return filteredModelList;
    }

    /**
     * Filter for adapter for shop
     * @param shops
     * @param query
     * @return
     */
  /*  public static List<Shop> shopFilter(List<Shop> shops, String query) {
        query = query.toLowerCase();
        final List<Shop> filteredModelList = new ArrayList<>();
        for (Shop sm : shops) {
            String name="",licNo="",holdingUid="",holdingNo="",holdingId="";
            if(sm.getName()!=null && !sm.getName().isEmpty()) {
                name = sm.getName().toLowerCase();
            }

            if(sm.getLicNo()!=null && !sm.getLicNo().isEmpty()) {
                licNo=sm.getLicNo().toLowerCase();
            }

            if(sm.getHoldingUid()!=null && !sm.getHoldingUid().isEmpty()) {
                holdingUid = sm.getHoldingUid();
            }

            if((sm.getHoldingUid()!=null && !sm.getHoldingUid().equals("0")) && sm.getHoldingNo()!=null) {
                holdingNo = sm.getHoldingNo().toLowerCase();
                holdingId = String.valueOf(sm.getHoldingUid()).toLowerCase();
                if (holdingUid.contains(query)||name.contains(query) || holdingNo.startsWith(query) || holdingId.startsWith(query) || licNo.contains(query)) {
                    filteredModelList.add(sm);
                }
            }else{
                if (name.contains(query) || licNo.contains(query)){
                    filteredModelList.add(sm);
                }
            }

        }
        return filteredModelList;
    }

    public static boolean checkGooglePlayServices(Context context){
        int checkGooglePlayServices = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context);
        if (checkGooglePlayServices != ConnectionResult.SUCCESS) {

            GoogleApiAvailability.getInstance().getErrorDialog((Activity) context,checkGooglePlayServices, REQUEST_CODE_RECOVER_PLAY_SERVICES).show();
            return false;
        }
        return true;
    }

    /**
     * Camera intent
     * @param context
     * @return path of the generated image
     */
    public static String dispatchTakePictureIntent(Context context) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(context.getPackageManager()) != null) {
            if (!new File(AppConfig.IMAGES_FOLDER).exists()){
                new File(AppConfig.IMAGES_FOLDER).mkdirs();
            }
            File photoFile=new File(AppConfig.IMAGES_FOLDER+ File.separator+ System.currentTimeMillis()+".jpg");
            if (photoFile != null) {
                Uri photoURI=null;
                if (Build.VERSION.SDK_INT>=24) {
                    photoURI = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", photoFile);
                }else{
                    photoURI = Uri.fromFile(photoFile);
                }
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                ((Activity)context).startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
            return photoFile.getPath();
        }
        return null;
    }


}
