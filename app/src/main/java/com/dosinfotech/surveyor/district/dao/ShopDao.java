package com.dosinfotech.surveyor.district.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.dosinfotech.surveyor.district.HoldingStatus;
import com.dosinfotech.surveyor.district.entity.Shop;

import java.util.List;

@Dao
public interface ShopDao {

  /*  @Insert(onConflict = OnConflictStrategy.REPLACE)
    long[] insertAllData(List<Shop> shops);
    @Query("SELECT * FROM shop WHERE surveyed=0")
    List<Shop> findAllData();
    @Query("SELECT COUNT(id) FROM shop WHERE ward=:ward")
    long countShopsByWard(int ward);
    @Query("SELECT * FROM shop WHERE ward=:ward")
    List<Shop> findShopsByWard(int ward);
    @Query("SELECT * FROM shop WHERE holding_uid=:uid AND surveyed=0")
    List<Shop> findShopsByHoldingUID(String uid);
    @Query("SELECT * FROM shop WHERE id IN (:ids)")
    List<Shop> findShopsByListIds(List<String> ids);
    @Query("SELECT * FROM (SELECT * FROM shop WHERE holding_uid LIKE '%,%' AND surveyed=0) WHERE holding_uid LIKE :uids")
    List<Shop> findShopsByMultipleHoldingUID(String uids);
    @Query("SELECT * FROM shop WHERE lic_no IN (:licNo)")
    List<Shop> findShopsByLicNo(String licNo);
    @Query("SELECT * FROM shop WHERE id IN (:ids)")
    List<Shop> findShopsForUpload(List<String> ids);
    @Query("UPDATE shop SET uploaded=1 WHERE id IN (:ids)")
    int updateOnUploadSuccess(List<String> ids);
    @Update
    int updateShop(Shop shop);
    @Query("SELECT * FROM shop WHERE surveyed=:surveyed AND ward=:ward AND status=:status AND uploaded=0 ORDER BY date(date_time) DESC")
    List<Shop> findShopsByWardAndSurveyedAndStatus(int ward, boolean surveyed, HoldingStatus status);
    @Query("SELECT * FROM shop WHERE surveyed=:surveyed AND ward=:ward ORDER BY date_time DESC")
    List<Shop> findShopsByWardAndSurveyed(int ward, boolean surveyed);
    @Query("SELECT * FROM shop WHERE status IS NULL AND surveyed=:surveyed AND ward=:ward ORDER BY date(date_time) DESC")
    List<Shop> findShopsByWardAndSurveyedAndStatusNull(int ward, boolean surveyed);
    @Delete
    int deleteShop(Shop shop);

    @Query("SELECT * FROM shop WHERE surveyed=:surveyed and uploaded=:uploaded and ward=:ward")
    List<Shop> findShopBywardAndUploaded(boolean surveyed, boolean uploaded, int ward);

    @Query("delete FROM shop WHERE ward=:ward")
    void removeData(int ward);
*/


}
