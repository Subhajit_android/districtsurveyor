package com.dosinfotech.surveyor.district.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.dosinfotech.surveyor.district.HoldingStatus;
import com.dosinfotech.surveyor.district.entity.BSUP;

import java.util.List;

@Dao
public interface BSUPDao {
   /* @Insert
    long[] insertAllData(List<BSUP> bsupList);

    @Query("SELECT * FROM bsup WHERE ward=:ward")
    List<BSUP> findAllDataByWard(int ward);

    @Query("SELECT COUNT(id) FROM bsup where ward=:wardNo")
    long countData(int wardNo);

    @Query("SELECT * FROM bsup WHERE gid IN (:gid)")
    List<BSUP> findAllByAllGid(List<String> gid);

    @Query("SELECT * FROM bsup WHERE surveyed=:surveyed AND ward=:ward ORDER BY id DESC")
    List<BSUP> findAllBSUPByWardAndSurveyed(int ward, boolean surveyed);

    @Query("SELECT * FROM bsup WHERE status=:status AND surveyed=:surveyed AND ward=:ward AND uploaded=0 ORDER BY id DESC")
    List<BSUP> findAllBSUPByWardAndSurveyedAndStatus(int ward, boolean surveyed, HoldingStatus status);

    @Update
    int updateBSUP(BSUP bsup);

    @Query("SELECT * FROM bsup WHERE gid=:gid")
    BSUP findBSUPByGid(int gid);

    @Query("SELECT * FROM bsup WHERE gid IN (:ids) AND uploaded=0")
    List<BSUP> findBSUPForUpload(List<String> ids);
    @Query("UPDATE bsup SET uploaded=1 WHERE gid IN (:ids)")
    int updateOnUploadSuccess(List<String> ids);

    @Query("SELECT * FROM bsup WHERE surveyed=:surveyed and uploaded=:uploaded and ward=:ward")
    List<BSUP> findBSUPBywardAndUploaded(boolean surveyed, boolean uploaded, int ward);

    @Query("delete FROM bsup WHERE ward=:ward")
    void removeData(int ward);*/
}

